import axios from 'axios'

export default {
  getAll () {
    return axios.get('/pics').then(res => res.data)
  }
}
